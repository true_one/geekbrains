# Build stage
FROM golang:1.15-alpine AS builder

ARG PROJECT_DIR=hello-world
WORKDIR /$PROJECT_DIR

ADD . /$PROJECT_DIR
RUN echo $PWD && ls -la
RUN go build -o /app/hello-world

# Final stage
FROM golang:1.15-alpine AS runtime-image

COPY --from=builder /app /app
WORKDIR /app

ENTRYPOINT ["./hello-world"]
